#!/bin/sh

for d in $(echo "$PATH" | tr ':' "\n"); do
  for f in $(ls "$d"); do
    [ -x "$d/$f" ] && list="$f:$list"
  done
done

prog=$(echo "$list" | tr ':' "\n" | dmenu -p "$*")

[ -n "$prog" ] && $prog $@
