highlight CursorLine cterm=bold
highlight ColorColumn ctermbg=darkblue
call matchadd('ColorColumn', '\%81v', 100)

set hid nonu noswf nohlsearch cul fo=tlq sw=0 ts=4 noet ls=2
set stl=%<%n:\ %f\ %M%R%H%W%Y\ %b,0x%B\ %=%l/%L\ %c%V(%o)\ %P

filetype plugin on
autocmd FileType * set fo-=c fo-=r fo-=o

colo desert
syntax on

let g:netrw_banner = 0
let g:netrw_winsize = 25

let g:go_fmt_command = "goimports"
